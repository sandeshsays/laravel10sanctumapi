
## About This API

The API allows users to do the following:
- Create a post
- Read all posts
- Edit  posts
- Delete posts
- Search for posts

## License

This repo is licensed under the [MIT license](https://opensource.org/licenses/MIT).
